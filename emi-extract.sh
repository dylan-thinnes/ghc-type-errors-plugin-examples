cd ./error-message-index
for file in message-index/messages/*/index.md; do echo $file | cut -d/ -f3 | grep -E 'GHC-[0-9]+' && grep -E '^summary:' "$file"; done | awk 'NR%2==1{name=$0}NR%2==0{gsub("^summary: ", ""); print name, $0}'
