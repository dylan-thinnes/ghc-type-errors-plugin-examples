{-# LANGUAGE RecordWildCards #-}
module ErrorsIndex where

import Prelude hiding ((<>))
import qualified Prelude as P ((<>))
import CustomErrorsTest hiding (plugin)

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..))
import GHC.Tc.Plugin (tcPluginIO)
import GHC.Tc.Utils.Monad (traceTc)
import GHC.Tc.Errors.Types
import GHC.Types.Error

import GHC.Utils.Outputable (text, quotes, hang, (<+>), (<>), ppr)
import Lens.Micro ((^?))

import Data.Maybe (maybeToList)

type HaskellErrorsIndex = [(String, String)]

getHeiMsg :: HaskellErrorsIndex -> TcRnMessage -> Maybe SDoc
getHeiMsg errorsIndex report = do
  DiagnosticCode{..} <- diagnosticCode report
  let strRep = diagnosticCodeNameSpace P.<> "-" P.<> show diagnosticCodeNumber
  summary <- lookup strRep errorsIndex
  pure $ hang
    (text "Additional information for error code" <+> quotes (text strRep) <> text ":")
    2
    (text summary)

plugin :: Plugin
plugin = defaultPlugin
  { errMsgPlugin = const $ Just $ ErrMsgPlugin
      { errMsgPluginInit = pure ()
      , errMsgPluginRewrite = \() -> \superTcRnMessage -> pure $ do
          tcRnMessage <- superTcRnMessage ^? underInfo
          heiMsg <- errorIndex `getHeiMsg` tcRnMessage
          pure $ AdditionalInformation [heiMsg] []
      , errMsgPluginStop = const (pure ())
      }
  }

-- Generated from front matter of errors with error codes
errorIndex :: HaskellErrorsIndex
errorIndex =
  [ ("GHC-00158", "An instance cannot be derived because the class is not stock deriveable.")
  , ("GHC-00482", "Lambda syntax appears in a pattern match.")
  , ("GHC-00711", "Duplicate deprecation warnings are illegal")
  , ("GHC-01239", "If expression used as function argument")
  , ("GHC-01629", "Too many type applications to a constructor in a pattern.")
  , ("GHC-03272", "The source file contains Unicode bidirectional formatting instructions")
  , ("GHC-04584", "There has been an attempt to pattern match on expression syntax. ")
  , ("GHC-04924", "'Unknown flag in {-# OPTIONS_GHC #-} pragma.'")
  , ("GHC-04956", "It is not possible to derive instances of argumentless type classes without the DeriveAnyClass extension.")
  , ("GHC-05380", "More than 1 constructor for a newtype")
  , ("GHC-05641", "Multiple Haddock comments for a single entity are not allowed")
  , ("GHC-05661", "A module cannot be imported using the qualified modifier both before and after the name of the module")
  , ("GHC-05989", "A type constructor is declared with more arguments than its kind")
  , ("GHC-06446", "Do notation in pattern match.")
  , ("GHC-07626", "Compiler not able to parse pattern. ")
  , ("GHC-09009", "Type roles should be one of representational, nominal, or phantom.")
  , ("GHC-09646", "There has been an attempt to pattern match on a tuple section.")
  , ("GHC-10190", "An enumeration would be empty.")
  , ("GHC-10333", "Generalized newtype deriving works only for types declared with the newtype keyword.")
  , ("GHC-11861", "No character literal provided within single quotes.")
  , ("GHC-11913", "Something other than a type class appears in a deriving statement.")
  , ("GHC-12003", "The type equality operator has not been imported into the current module.")
  , ("GHC-13218", "A linear function was used for a higher kind, which is not allowed")
  , ("GHC-20125", "Initialization of record with missing field(s).")
  , ("GHC-20825", "Record update syntax requires that at least one field be specified.")
  , ("GHC-21231", "The numeric escape sequence represents a number that is too large")
  , ("GHC-24180", "A type operator was not provided with both arguments.")
  , ("GHC-25078", "An invalid operator precedence was provided.")
  , ("GHC-25897", "A pattern match on a GADT cannot succeed unless GHC knows the result type of the pattern match.")
  , ("GHC-27207", "Lazy pattern in expression context.")
  , ("GHC-28007", "LANGUAGE pragmas should come before the module declaration.")
  , ("GHC-30606", "A binding has constraints that are redundant.")
  , ("GHC-31574", "The linear function type is used, but LinearTypes are not enabled")
  , ("GHC-39999", "An expression requires a type class instance which is not provided by the context.")
  , ("GHC-40798", "An optional warning for detecting usage of infix, suffix or prefix operators that could be parsed differently in future due to whitespace.")
  , ("GHC-42044", "GHC didn't recognize pragma and will thus ignore it")
  , ("GHC-44432", "A type signature was provided, but no binding was given.")
  , ("GHC-45696", "If-Then-Else expression in pattern match.")
  , ("GHC-46537", "GHC failed to recognize name of a language extension")
  , ("GHC-46956", "A local kind variable was used to classify a type from a scope in which the kind variable is not available.")
  , ("GHC-47535", "An identifier does not refer to a record selector but is used as such.")
  , ("GHC-47854", "An identifier appears twice in an export list.")
  , ("GHC-48099", "Top-level bindings may not be strict, and they may not have unlifted types.")
  , ("GHC-48361", "You can only bind value-level variables in a pattern, not type variables.")
  , ("GHC-49957", "A promoted data constructor was used as a type without it being indicated with a tick mark.")
  , ("GHC-51179", " Missing LambdaCase language extension")
  , ("GHC-53633", "A pattern is impossible to reach due to earlier patterns")
  , ("GHC-53786", "A pattern contains case-of syntax.")
  , ("GHC-54540", "It is not possible to derive a typeclass instance if the constructors of the type are not in scope")
  , ("GHC-55666", "Using a strictness annotation (bang) on an unlifted type is redudant as unlifted values are strict by definition")
  , ("GHC-56538", "A type class instance declaration is declared for something that is not a type class.")
  , ("GHC-58008", "Pattern matching on GADTs without MonoLocalBinds is fragile")
  , ("GHC-58481", "Generic parsing error.")
  , ("GHC-59840", "GHC does not support GADTs or type families which witness equality of multiplicities")
  , ("GHC-62016", "An instance cannot be derived because the kinds cannot be made to match.")
  , ("GHC-62161", "Pattern match(es) are non-exhaustive.")
  , ("GHC-62330", "Float and integer literals cannot contain underscores.")
  , ("GHC-64088", "Use of \"forall\" as an identifier is discouraged")
  , ("GHC-64725", "An invalid constraint or type family reduces to a custom type error.")
  , ("GHC-66228", "A view pattern was used in an expression, rather than a pattern.")
  , ("GHC-68686", "The arguments to the LANGUAGE pragma could not be parsed")
  , ("GHC-69158", "Different identifiers with the same name are (re-)exported from the same module.")
  , ("GHC-69925", "Illegal unboxed string literal in pattern.")
  , ("GHC-70712", "Double-dot syntax is not allowed in a record update.")
  , ("GHC-71614", "A lambda expression must have at least one parameter.")
  , ("GHC-75356", "An export item suggests that (in-scope) constructors or class methods exist when they do not.")
  , ("GHC-76037", " An identifier is not in scope.")
  , ("GHC-77037", "Items brought into scope are not listed explicitly.")
  , ("GHC-77539", "A tuple of constraints was used without enabling the ConstraintKinds extension")
  , ("GHC-78892", "An attempt has been made to use a let expression whilst pattern matching. ")
  , ("GHC-80768", "It is an error to use type families in the return kind of a class")
  , ("GHC-83865", "You provided a value of a given type, whereas GHC expected a different type.")
  , ("GHC-84077", "A type application with @ does not have a space before.")
  , ("GHC-87429", "Constraints present in datatype declaration without DatatypeContexts.")
  , ("GHC-87491", "qualified after the module requires the ImportQualifiedPost extension.")
  , ("GHC-88464", "An unknown variable name was referenced.")
  , ("GHC-90177", "An instance was defined separately from its type or class.")
  , ("GHC-90584", "The type class `Typeable` does not need to be derived.")
  , ("GHC-91938", "Each equation in a function definition must have the same number of arguments.")
  , ("GHC-92994", "A foreign function import uses a higher-rank type.")
  , ("GHC-93557", "Illegal typeclass instance")
  , ("GHC-94458", "A Haddock comment appears in an illegal position")
  , ("GHC-94817", "A tab character occurred in the input file.")
  , ("GHC-95644", "Bang pattern in expression context.")
  , ("GHC-95781", "The expression cannot be applied to the given type argument.")
  , ("GHC-95909", "Constructor was not instantiated with required strict field(s).")
  , ("GHC-97044", "Type class does not allow user-specified instances")
  , ("GHC-97441", "Literal overflowing range of supported values")
  , ("GHC-97739", "Kind arguments must occur prior to the types that they classify.")
  , ("GHC-98980", "Arrow command syntax was used in a pattern.")
  , ("GHC-99623", "An import item suggests that (in-scope) constructors or class methods exist when they do not.")
  ]
