{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedRecordDot #-}
module LensMonoidInstance where

import CustomErrorsTest

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..), TcPluginM)
import GHC.Tc.Plugin (tcPluginIO)
import GHC.Tc.Errors.Types
import GHC.Types.Error

import GHC.Utils.Outputable (text)

import qualified Data.List.NonEmpty as NE
import Data.Maybe (maybeToList)

import GHC.Types.Name (occNameString, nameOccName, nameModule_maybe)
import GHC.Core.TyCon (isClassTyCon, tyConName)
import GHC.Core.Type (tcSplitTyConApp_maybe)
import GHC.Unit.Types (moduleUnitId, baseUnitId, unitIdFS)
import GHC.Tc.Types.Constraint (CtLoc(..))
import GHC.Tc.Types.Origin (CtOrigin(..))
import GHC.Data.FastString (unpackFS)
import Data.List (isPrefixOf)
import GHC.Types.Unique.FM (emptyUFM)
import GHC.Utils.Outputable (text, vcat)
import GHC.Tc.Utils.Monad (reportDiagnostic)
import GHC.Utils.Error (mkPlainErrorMsgEnvelope)
import GHC (noSrcSpan, moduleNameString, moduleName)
import Data.Char (isDigit)
import Control.Monad.IO.Class (liftIO)
import System.Directory (getCurrentDirectory)
import Lens.Micro ((^.))

plugin :: Plugin
plugin = mkSimplePlugin modifyReport
  where
  modifyReport :: TcRnMessage -> TcPluginM [SDoc]
  modifyReport message = pure $ case message ^. underInfo of
    TcRnSolverReport reportWithCtxt reason ->
      case reportContent reportWithCtxt of
        CannotResolveInstance { cannotResolve_item = item }
          | filterItem item ErrorItemFilter
              { filterPackageName = is "microlens"
              , filterModuleName = const True
              , filterHeadName = is "Monoid"
              }
          -> pure $ vcat ["You may have tried to use a prism as a lens.", "Consider replacing infallible operators like ‘(^.)’ with fallible operators like ‘(^?)’"]
        _ -> []
    _ -> []
