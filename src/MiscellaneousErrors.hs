{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedRecordDot #-}
module MiscellaneousErrors where

import CustomErrorsTest

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..), TcPluginM)
import GHC.Tc.Plugin (tcPluginIO)
import GHC.Tc.Errors.Types
import GHC.Types.Error

import GHC.Utils.Outputable (text)

import qualified Data.List.NonEmpty as NE
import Data.Maybe (maybeToList)

import GHC.Types.Name (occNameString, nameOccName, nameModule_maybe)
import GHC.Core.TyCon (isClassTyCon, tyConName)
import GHC.Core.Type (tcSplitTyConApp_maybe)
import GHC.Unit.Types (moduleUnitId, baseUnitId, unitIdFS)
import GHC.Tc.Types.Constraint (CtLoc(..))
import GHC.Tc.Types.Origin (CtOrigin(..))
import GHC.Data.FastString (unpackFS)
import Data.List (isPrefixOf)
import GHC.Types.Unique.FM (emptyUFM)
import GHC.Utils.Outputable (text)
import GHC.Tc.Utils.Monad (reportDiagnostic)
import GHC.Utils.Error (mkPlainErrorMsgEnvelope)
import GHC (noSrcSpan, moduleNameString, moduleName)
import Data.Char (isDigit)
import Control.Monad.IO.Class (liftIO)
import System.Directory (getCurrentDirectory)
import Lens.Micro ((^.))

plugin :: Plugin
plugin = mkSimplePlugin modifyReport
  where
  modifyReport :: TcRnMessage -> TcPluginM [SDoc]
  modifyReport message = pure $ case message ^. underInfo of
    TcRnSolverReport reportWithCtxt reason ->
      case reportContent reportWithCtxt of
        Mismatch { mismatchMsg = CouldNotDeduce { cnd_wanted = wanted } } ->
          case wanted of
            (_head NE.:| []) -> pure "Could not deduce: Single"
            _ -> pure "Could not deduce: Multiple"
        CannotResolveInstance { cannotResolve_item = item }
          -- Foo detector
          | filterItem item ErrorItemFilter
              { filterPackageName = is "use-test-plugin"
              , filterModuleName = const True
              , filterHeadName = is "Foo"
              }
          -> pure "FOO is not allowed for non-ints"

          -- Every other unresolved instance
          | otherwise -> []
        _ -> []
    _ -> []
