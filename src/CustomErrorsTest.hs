{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE RecordWildCards #-}
module CustomErrorsTest where

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..), TcPluginM)
import GHC.Tc.Errors.Types
import GHC.Types.Error

import qualified Data.Typeable as Typeable

import GHC.Types.Name (occNameString, nameOccName, nameModule_maybe)
import GHC.Core.TyCon (isClassTyCon, tyConName)
import GHC.Core.Type (tcSplitTyConApp_maybe)
import GHC.Unit.Types (moduleUnitId, baseUnitId, unitIdFS)
import GHC.Tc.Types.Constraint (CtLoc(..))
import GHC.Tc.Types.Origin (CtOrigin(..))
import GHC.Data.FastString (unpackFS)
import Data.List (isPrefixOf)
import GHC.Types.Unique.FM (emptyUFM)
import GHC.Utils.Outputable (text, hcat)
import GHC.Tc.Utils.Monad (reportDiagnostic)
import GHC.Utils.Error (mkPlainErrorMsgEnvelope)
import GHC (noSrcSpan, moduleNameString, moduleName)
import Data.Char (isDigit)
import Control.Monad.IO.Class (liftIO)
import System.Directory (getCurrentDirectory)
import Lens.Micro ((^?))

underInfo :: Functor f => (TcRnMessage -> f TcRnMessage) -> TcRnMessage -> f TcRnMessage
underInfo f (TcRnMessageWithInfo detailed (TcRnMessageDetailed errInfo message)) =
  TcRnMessageWithInfo detailed . TcRnMessageDetailed errInfo <$> underInfo f message
underInfo f (TcRnMessagePluginRewritten message rewrite) =
  flip TcRnMessagePluginRewritten rewrite <$> underInfo f message
underInfo f other = f other

stripRewrites :: TcRnMessage -> TcRnMessage
stripRewrites (TcRnMessageWithInfo detailed (TcRnMessageDetailed errInfo message)) =
  TcRnMessageWithInfo detailed (TcRnMessageDetailed errInfo (stripRewrites message))
stripRewrites (TcRnMessagePluginRewritten message rewrite) =
  stripRewrites message
stripRewrites other = other

additionalDepth :: TcRnMessage -> Int
additionalDepth (TcRnMessageWithInfo detailed (TcRnMessageDetailed errInfo message)) =
  additionalDepth message
additionalDepth (TcRnMessagePluginRewritten message rewrite) =
  additionalDepth message + 1
additionalDepth other = 0

mkSimplePlugin :: (TcRnMessage -> TcPluginM [SDoc]) -> Plugin
mkSimplePlugin handler = defaultPlugin
  { errMsgPlugin = const $ Just $ ErrMsgPlugin
      { errMsgPluginInit = pure ()
      , errMsgPluginRewrite = \_ superTcRnMessage ->
          case superTcRnMessage ^? underInfo of
            Nothing -> pure Nothing
            Just tcRnMessage -> do
              newBullets <- handler tcRnMessage
              pure $ if null newBullets then Nothing else Just (AdditionalInformation newBullets [])
      , errMsgPluginStop = const (pure ())
      }
  }

data CustomDiagnostic a = CustomDiagnostic String
  deriving (Show, Eq, Ord, Typeable.Typeable)

instance HasDefaultDiagnosticOpts a => Diagnostic (CustomDiagnostic a) where
  type DiagnosticOpts (CustomDiagnostic a) = a
  diagnosticMessage _opts (CustomDiagnostic msg) = mkSimpleDecorated $ text msg
  diagnosticReason _msg = ErrorWithoutFlag
  diagnosticHints _msg = []
  diagnosticCode _msg = Nothing

data ErrorItemFilter = ErrorItemFilter
  { filterPackageName :: String -> Bool
  , filterModuleName :: String -> Bool
  , filterHeadName :: String -> Bool
  }

data ErrorItemInfo = ErrorItemInfo
  { infoPackageName :: String
  , infoModuleName :: String
  , infoHeadName :: String
  }
  deriving (Show, Eq, Ord)

is :: String -> String -> Bool
is = (==)

filterItem :: ErrorItem -> ErrorItemFilter -> Bool
filterItem item filter =
  case extractErrorItemInfo item of
    Just info -> filterErrorItemInfo info filter
    Nothing -> False

filterErrorItemInfo :: ErrorItemInfo -> ErrorItemFilter -> Bool
filterErrorItemInfo info filter =
  filter.filterPackageName info.infoPackageName &&
  filter.filterModuleName info.infoModuleName &&
  filter.filterHeadName info.infoHeadName

extractErrorItemInfo :: ErrorItem -> Maybe ErrorItemInfo
extractErrorItemInfo item
  | let ct_loc = errorItemCtLoc item
  , OccurrenceOf name <- ctl_origin ct_loc
  , Just ctMod <- nameModule_maybe name
    -- Get package name
  , let package_id = unitIdFS (moduleUnitId ctMod)
  , let package_name = init (takeWhile (not . isDigit) (unpackFS package_id))
    -- Get module name
  , let module_name = moduleNameString (moduleName ctMod)
    -- Identify uninstantiated constraint as <headName> <...any arguments>
  , Just (tyConHead, _args) <- tcSplitTyConApp_maybe (ei_pred item)
  , isClassTyCon tyConHead
  -- Get headName
  , let headName = occNameString $ nameOccName $ tyConName tyConHead
  = Just $ ErrorItemInfo package_name module_name headName
  | otherwise
  = Nothing
