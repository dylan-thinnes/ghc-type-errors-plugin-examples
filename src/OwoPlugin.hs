{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedRecordDot #-}
module OwoPlugin where

import Owoifier

import CustomErrorsTest

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..), TcPluginM)
import GHC.Tc.Plugin (tcPluginIO)
import GHC.Tc.Errors.Types
import GHC.Types.Error

import GHC.Utils.Outputable (text, defaultSDocContext)

import qualified Data.List.NonEmpty as NE
import Data.Maybe (maybeToList)

import GHC.Types.Name (occNameString, nameOccName, nameModule_maybe)
import GHC.Core.TyCon (isClassTyCon, tyConName)
import GHC.Core.Type (tcSplitTyConApp_maybe)
import GHC.Unit.Types (moduleUnitId, baseUnitId, unitIdFS)
import GHC.Tc.Types.Constraint (CtLoc(..))
import GHC.Tc.Types.Origin (CtOrigin(..))
import GHC.Data.FastString (unpackFS)
import Data.List (isPrefixOf)
import GHC.Types.Unique.FM (emptyUFM)
import GHC.Utils.Outputable (text, showPprUnsafe)
import GHC.Tc.Utils.Monad (reportDiagnostic)
import GHC.Utils.Error (mkPlainErrorMsgEnvelope, formatBulleted)
import GHC (noSrcSpan, moduleNameString, moduleName)
import Data.Char (isDigit)
import Control.Monad.IO.Class (liftIO)
import System.Directory (getCurrentDirectory)
import Lens.Micro ((^.))
import qualified Data.Text as T (pack, unpack)

plugin :: Plugin
plugin = defaultPlugin
  { errMsgPlugin = const $ Just $ ErrMsgPlugin
      { errMsgPluginInit = pure ()
      , errMsgPluginRewrite = \_ tcRnMessage -> do
          let originalMsg = diagnosticMessage (defaultDiagnosticOpts @TcRnMessage) tcRnMessage
          let owoMsg = T.unpack $ owoify $ T.pack $ showPprUnsafe $ formatBulleted originalMsg
          pure $ Just $ UnknownChange $ TcRnUnknownMessage $ UnknownDiagnostic id $ CustomDiagnostic @TcRnMessageOpts owoMsg
      , errMsgPluginStop = const (pure ())
      }
  }
