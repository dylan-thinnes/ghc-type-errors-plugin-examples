{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedRecordDot #-}
module RecordsHasFieldErrors where

import Prelude hiding ((<>))

import CustomErrorsTest

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..), TcPluginM)
import GHC.Tc.Plugin (tcPluginIO, tcPluginTrace)
import GHC.Tc.Errors.Types
import GHC.Types.Error

import GHC.Utils.Outputable (text, showPprUnsafe, ppr)

import qualified Data.List.NonEmpty as NE
import Data.Maybe (maybeToList)

import GHC.Types.Name (occNameString, nameOccName, nameModule_maybe, mkOccNameFS, varName)
import GHC.Types.Name.Reader (lookupGRE, WhichGREs(..), LookupGRE(..), gre_par, GlobalRdrElt(..), Parent(..))
import GHC.Core.TyCon (isClassTyCon, tyConName)
import GHC.Core.Type (tcSplitTyConApp_maybe)
import GHC.Unit.Types (moduleUnitId, baseUnitId, unitIdFS)
import GHC.Tc.Types.Constraint (CtLoc(..))
import GHC.Tc.Types.Origin (CtOrigin(..))
import GHC.Data.FastString (FastString, unpackFS)
import Data.List (isPrefixOf)
import GHC.Types.Unique.FM (emptyUFM)
import GHC.Utils.Outputable (text, (<+>), (<>), pprQuotedList, quotes, ftext, vcat, hang)
import GHC.Tc.Utils.Monad (reportDiagnostic, getGlobalRdrEnv)
import GHC.Utils.Error (mkPlainErrorMsgEnvelope)
import GHC (noSrcSpan, moduleNameString, moduleName, Type)
import Data.Char (isDigit)
import Control.Monad.IO.Class (liftIO)
import System.Directory (getCurrentDirectory)
import Lens.Micro ((^.))
import GHC.Core.TyCo.Rep (Type(..), TyLit(..))

extractHasField :: ErrorItem -> Maybe (Type, FastString, Type)
extractHasField item
    -- Identify uninstantiated constraint as <headName> <...any arguments>
  | Just (tyConHead, args) <- tcSplitTyConApp_maybe (ei_pred item)
  , isClassTyCon tyConHead
  -- Make sure headName is HasField
  , let headName = occNameString $ nameOccName $ tyConName tyConHead
  , headName == "HasField"
  -- Make sure arguments are [_, fieldName, structType, fieldType]
  , [_, LitTy (StrTyLit fieldNameFS), structType, fieldType] <- args
  = Just (structType, fieldNameFS, fieldType)
  | otherwise
  = Nothing

plugin :: Plugin
plugin = mkSimplePlugin modifyReport
  where
  modifyReport :: TcRnMessage -> TcPluginM [SDoc]
  modifyReport message = case message ^. underInfo of
    TcRnSolverReport reportWithCtxt reason ->
      case reportContent reportWithCtxt of
        CannotResolveInstance { cannotResolve_item = item } ->
          case extractHasField item of
            Just (structType, fieldNameFS, fieldType) -> do
              glb_env <- unsafeTcPluginTcM getGlobalRdrEnv
              let structsWithThisField =
                    [ parent
                    | occ <- lookupGRE glb_env (LookupOccName (mkOccNameFS varName fieldNameFS) SameNameSpace)
                    , ParentIs parent <- [gre_par occ]
                    ]
              pure $ pure $
                case structsWithThisField of
                  [] ->
                    hang
                      ("The field" <+> quotes (ftext fieldNameFS) <+> "is not defined on" <+> ppr structType)
                      2
                      ("It is not a field of any datatype in scope - is this field name a typo?")
                  [oneStruct] ->
                    hang
                      ("The field" <+> quotes (ftext fieldNameFS) <+> "is not defined on" <+> ppr structType)
                      2
                      ("It is a field on" <+> quotes (ppr oneStruct) <> ". Is that the type you were targeting?")
                  manyStructs ->
                    hang
                      ("The field" <+> quotes (ftext fieldNameFS) <+> "is not defined on" <+> ppr structType)
                      2
                      ("It is a field on the following datatypes:" <+> pprQuotedList manyStructs)
            Nothing -> pure []
        _ -> pure []
    _ -> pure []
