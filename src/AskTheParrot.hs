{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
module AskTheParrot where

import Prelude hiding ((<>))
import qualified Prelude as P ((<>))
import CustomErrorsTest

import GHC.Driver.Plugins
import GHC.Tc.Types (TcPlugin(..), TcPluginSolver, unsafeTcPluginTcM, TcPluginSolveResult(..), ErrMsgPlugin(..))
import GHC.Tc.Plugin (tcPluginIO)
import GHC.Tc.Utils.Monad (traceTc)
import GHC.Tc.Errors.Types
import GHC.Types.Error
import GHC.Utils.Error (formatBulleted)

import GHC.Utils.Outputable (text, quotes, hang, (<+>), (<>), ppr, showPprUnsafe, hsep, vcat)
import Lens.Micro ((^.), (&))

import Data.Maybe (maybeToList)
import System.Process (readProcessWithExitCode)
import System.Exit (ExitCode(..), exitFailure)
import Data.Char (isSpace)
import System.IO (hClose, openTempFile, hPutStr)
import qualified System.Directory

-- because most of the ecosystem is not yet updated for the compiler, rely on a
-- shell script instead of using aeson and http
-- Requires `jq`
simpleRequestScript :: String
simpleRequestScript = unlines
  [ "#!/usr/bin/env bash"
  , "jq -sR \\"
  , "  '{"
  , "     \"model\": \"gpt-3.5-turbo\","
  , "     \"messages\": [{\"role\": \"user\", \"content\": .}],"
  , "     \"temperature\": 0.7"
  , "   }' | tee /dev/stderr | \\"
  , "  curl https://api.openai.com/v1/chat/completions \\"
  , "    -H \"Content-Type: application/json\" \\"
  , "    -H \"Authorization: Bearer $1\" \\"
  , "    -d @- | tee /dev/stderr | \\"
  , "  jq -r '.choices[0].message.content'"
  ]

completeViaChatGPT :: String -> String -> IO (Maybe String)
completeViaChatGPT apiKey question = do
  --putStrLn apiKey
  --putStrLn question
  (scriptFile, handle) <- openTempFile "/tmp" "chatgpt-script-file"
  hPutStr handle simpleRequestScript
  hClose handle
  System.Directory.setPermissions scriptFile $ System.Directory.emptyPermissions
    & System.Directory.setOwnerExecutable True
    & System.Directory.setOwnerWritable True
    & System.Directory.setOwnerReadable True
    & System.Directory.setOwnerSearchable True
  (exitCode, response, stderr) <- do
    readProcessWithExitCode scriptFile [apiKey] question
  --putStrLn response
  --putStrLn stderr
  pure $ case exitCode of
    ExitSuccess | not (null response) -> Just response
    _ -> Nothing

reflow :: String -> SDoc
reflow src = vcat $ map (hsep . map text . words) $ lines src

plugin :: Plugin
plugin = defaultPlugin
  { errMsgPlugin = \args -> Just $ ErrMsgPlugin
      { errMsgPluginInit = tcPluginIO $ do
          case args of
            [] -> do
              putStrLn "Please specify an OpenAI API key via -fplugin-opt=AskTheParrot:<apiKey>"
              _ <- exitFailure
              pure ""
            [apiKey] -> pure apiKey
            _ -> do
              putStrLn "Too many args from -fplugin-opt=AskTheParrot:"
              _ <- exitFailure
              pure ""
      , errMsgPluginRewrite = \apiKey -> \superTcRnMessage -> do
          let originalMsg = diagnosticMessage (defaultDiagnosticOpts @TcRnMessage) (stripRewrites superTcRnMessage)
              originalMsgText = showPprUnsafe $ formatBulleted originalMsg
              question =
                "I just got the following error while writing a Haskell program. Could you explain in simple terms what it means?"
                ++ "\n\n" ++ originalMsgText
          mbResponse <- tcPluginIO $ completeViaChatGPT apiKey question
          pure $ do
            response <- mbResponse
            let doc = hang
                  (text "ChatGPT's advice about this error:")
                  2
                  (reflow response)
            pure $ AdditionalInformation [doc] []
      , errMsgPluginStop = const (pure ())
      }
  }
