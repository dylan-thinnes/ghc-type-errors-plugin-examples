module LensAsPrism where

import Lens.Micro

prismAsLens :: Int
prismAsLens =  (undefined :: (Maybe Int, Char)) ^. _1 . _Just
