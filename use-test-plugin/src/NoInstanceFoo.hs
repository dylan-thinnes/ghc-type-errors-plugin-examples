module NoInstanceFoo where

class Foo a where foo :: a -> String
instance Foo Int where foo = show

couldntFindInstanceFoo :: String
couldntFindInstanceFoo = "Foo for char: " <> foo 'a'
