{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE KindSignatures #-}
module CustomFieldsMechanism where

data Proxy (x :: p) = Proxy

data Complex = Int :+ Int

class GetField (t :: *) (s :: Symbol) a | t s -> a where
  get :: t -> Proxy s -> a

instance GetField Complex "r" Double where
  get (r :+ _) _ = r

instance GetField Complex "i" Double where
  get (_ :+ i) _ = i

i :: Proxy "i"
i = Proxy

r :: Proxy "r"
r = Proxy

theta :: Proxy "theta"
theta = Proxy

magnitude :: Complex -> Double
magnitude c = sqrt $ c `get` i * c `get` i + c `get` r * c `get` r
