{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
module Singletons where

data family SomeConstraint s
data instance SomeConstraint "a" = A Int
data instance SomeConstraint "b" = B String
data instance SomeConstraint "c" = C Bool
