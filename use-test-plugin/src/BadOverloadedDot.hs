{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE DuplicateRecordFields #-}
module BadOverloadedDot where

data Point2D = Point2D { x, y :: Double }
  deriving (Show, Eq, Ord)

data Point3D = Point3D { x, y, z :: Double }
  deriving (Show, Eq, Ord)

data Measure = Measure { amount :: Integer, unit :: String }
  deriving (Show, Eq, Ord)

p2 :: Point2D
p2 = Point2D 2 5

p3 :: Point3D
p3 = Point3D 1 3 7

measure :: Measure
measure = Measure 10 "furlongs"

example :: Double
example
    -- successful field retrieval
  = p2.x + p2.y
    -- field does not exist on p2 but does exist on p3
  + p2.z
    -- field does not exist on any record
  + p3.nonExistentField
    -- field does not exist on measure but does exist on p2, p3
  + measure.x

  -- ignore this for now, since it override other errors, and its error message
  -- is already pretty good
  -- + measure.amount -- field exists, but has wrong return type
