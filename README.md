# Ghc Type Errors Plugin Examples

This is a repository for testing possibilities provided by the error messages
rewriting plugins interface. There are a few different example plugins available.

## Usage

- Clone ghc-type-errors-plugin and this repository to be alongside one another.

  ```
  $ git clone https://gitlab.haskell.org/dylan-thinnes/ghc-type-errors-plugin.git
  $ git clone https://gitlab.haskell.org/dylan-thinnes/ghc-type-errors-plugin-examples.git
  ```

- Run the [normal build process](https://gitlab.haskell.org/ghc/ghc/-/wikis/building) for GHC in the `ghc-type-errors-plugin` directory.

- Modify the `with-project:` field in `cabal.project` to the absolute path `<location of ghc-type-errors-plugin repository>/_build/stage1/bin/ghc`.

- Then, run `cabal build` in the `ghc-type-errors-plugin-examples/use-test-plugin`.
  This should build the plugins available in `ghc-type-errors-plugin-examples`,
  and then try to build some modules in `use-test-plugin` using some those
  plugins. You can modify which modules and plugins are being used in the
  `use-test-plugin.cabal` file.
